## Accelerating data processors
Data processors need to be rewritten to be accelerated. These days, it is good practice to maintain a code that runs hybrid (CPU or GPU, depending on the hardware), i.e. a "multi-block-lattice data processor" and an "accelerated-lattice" data processor, because the former performs better on CPU and the latter does a better job on GPU. As compiler technology, hardware, and Palabos evolve, we do believe that the accelerated lattice will prevail ultimately for any type of platform. So for, just write two data processors which express the loop over space indices differently (sequential loop for the multi-block-lattice, concurrent loop for the accelerated one). For maintenance reasons, you can still express the body of the loop in a separate template that is called from both versions of the data processor.

As an example, here are two data processors which initialize the domain to equilibrium populations with space-dependent velocity and density, provided through a templated function object. The first one is a traditional data processor for the multi-block lattice. The class `CustomEquilibriumFunctional3D` inherits from `BoxProcessingFunctional3D_L`.

    template<typename T, template<class U> class Descriptor, class RhoUFunction>
    void CustomEquilibriumFunctional3D<T,Descriptor,RhoUFunction>::process (
            Box3D domain, AtomicAcceleratedLattice3D<T,Descriptor>& lattice)
    {
        Dot3D relativeOffset = lattice.getLocation();
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                for (plint iZ=domain.z0; iZ<=domain.z1; ++iZ) {
                    T rho;
                    Array<T, 3> j;
                    f(iX + relativeOffset.x, iY + relativeOffset.y, iZ + relativeOffset.z, rho, j);
                    j *= rho;
                    T jSqr = VectorTemplate<T,Descriptor>::normSqr(j);
                    Cell<T,Descriptor>& cell = lattice.get(iX, iY, iZ);
                    for (int iPop = 0; iPop < Descriptor<T>::numPop; ++iPop) {
                        cell[iPop] = cell.computeEquilibrium(iPop, Descriptor<T>::rhoBar(rho), j, jSqr);
                    }
                }
            }
        }
    }

In the second example, a data processor `AcceleratedO2Equilibrium3D` is written which inherits from `BoxProcessingFunctional3D_A`. It is accelerated and acts directly on an accelerated lattice:

           template<typename T, template<class U> class Descriptor, class RhoUFunction>
           void AcceleratedO2Equilibrium3D<T,Descriptor,RhoUFunction>::process (
                   Box3D domain, AtomicAcceleratedLattice3D<T,Descriptor>& lattice)
           {
               Dot3D relativeOffset = lattice.getLocation();
    /* 1 */    lattice.for_each(domain,
    /* 2 */         [relativeOffset, lattice](plint i, plint iX, plint iY, plint iZ, int collisionModel)
                    {
                       T rho;
                       Array<T, 3> j;
                       f(iX + relativeOffset.x, iY + relativeOffset.y, iZ + relativeOffset.z, rho, j);
                       j *= rho;
                       T jSqr = VectorTemplate<T,Descriptor>::normSqr(j);
                       T rhoBar = Descriptor<T>::rhoBar(rho);
                       T invRho = Descriptor<T>::invRho(rhoBar);
    /* 3 */            Array<T, Descriptor<T>::q> pop;
                       for (int iPop = 0; iPop < Descriptor<T>::numPop; ++iPop) {
    /* 4 */                 pop[iPop] = Descriptor<T>::bgk_ma2_equilibrium(iPop, rhoBar, invRho, j, jSqr);
                       }
    /* 5 */            lattice.pushPop(i, pop);
                   });
           }

Here are some comments on this data processor:
- `/* 1 */` The `AtomicAcceleratedLattice3D` has a method `for_each` which is similar to the STL `for_each` algorithm and offers a way to loop over  all cells of the local lattice. The loop is carried out concurrently (on GPU, if the compiler allows to do so).
- `/* 2 */` The `for_each` function is provided with a function object, here a lambda function. The function receives a linear index `i` into the cells, as well as three Euclidean coordinates `iX`, `iY`, and `iZ`. The last argument is an identifier of the collision model (the constants are defined in `src/acceleratedLattice/acceleratedDefinitions.h`. You are going to need this constant to execute actions depending on the type of collision model, because you can't currently execute virtual functions from the GPU.
- `/* 3 */` In memory, the populations are spread out in a non-consecutive way, due to the structure-of-array layout. But before writing the data back to memory, we store it in a local array.
- `/* 4 */` Equilibrium cannot be computed, as usual in the multi-block lattice, through a virtual function call in the dynamics object. In this example, we simply compute a second-order equilibrium, discarding the fact that the collision model might actually require a higher-order one. The accelerated-lattice framework is less flexible than the multi-block-lattice one. To adapt the type of equilibrium to the collision model, you can either write different data processors, or add an `if` statement to react to the collision model.
- `/* 5 */` Data is written to memory in a push operation (and retrieved from memory in a pull operation).

In the following, we show an example of a reductive data processor which computes the sum of the elements of a scalar field over a sub-domain. The non-accelerated, classic `BoxScalarSumFunctional3D` inherits from `ReductiveBoxProcessingFunctional3D_S`

    template<typename T>
    void BoxScalarSumFunctional3D<T>::process (
            Box3D domain, ScalarField3D<T>& scalarField )
    {
        BlockStatistics& statistics = this->getStatistics();
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                for (plint iZ=domain.z0; iZ<=domain.z1; ++iZ) {
                    statistics.gatherSum(sumScalarId, (double)scalarField.get(iX,iY,iZ));
                }
            }
        }
    }

There exists no specific "accelerated" version of the scalar-field, or the tensor-field, both of which are fit to be used on the GPU out of the box. The corresponding accelerated data processor thus inherits from `ReductiveBoxProcessingFunctional3D_S`just the same. It replaces the nested sums by a call to `for_each_reduce`which includes a reduction operation:

    template<typename T>
    void AccScalarSumFunctional3D<T>::process (
            Box3D domain, ScalarField3D<T>& scalarField )
    {
        T sumval = scalarField.for_each_reduce(domain, (T)0, std::plus<T>(),
            [&scalarField](plint i, plint iX, plint iY, plint iZ, T value)
        {
            return value;
        });
        this->getStatistics().gatherSum(sumScalarId, (double)sumval);
    }

It is not possible to call `gatherSum` from within the concurrent kernel, because the Palabos statistics class is not thread safe (or for that matter, optimized for use on an accelerator).
