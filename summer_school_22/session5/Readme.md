## Session 5

For this session, there is nothing to change in the code. You will familiarize yourself with the main code of the Palabos Summer School 2022, the carbon sequestration. There is nothing to change. You will simply execute the code, retrieve the VTK files produces in the `tmp` directory, and visualize the data locally on your computer.

If you are working in a machine with multiple GPUs, select a specific GPU to avoid conflict with others. For example: 

    export CUDA_VISIBLE_DEVICES=3

to select the fourth GPU.
