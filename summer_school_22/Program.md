# Palabos Summer School 2022
## Organization
Arrival: Monday 8:30 - 9:00
Venue:  Battelle building A - Route de Drize 7 - 1227 Carouge (ground floor, left)
Dinner:  Tuesday 19:00

## Schedule

|       | 9:00-10:45 | 10:45 | 11:00-12:00  | 12:00-13:30 | 13:30 - 15:00 | 15:00 | 15:15  - 17:00 |
| - | - | :-: | - | - | - | :-: | - |
| Mo | Session 1   | Coffee Br.    | Session 2    | Lunch | Session 3 | Coffee Br. | Session 4 |
| Tu | Session 5   | Coffee Br.    | Session 6    | Lunch | Session 7 | Coffee Br. | Session 8 |
| We | Session 9   | Coffee Br.    | Session 10    | Lunch | Session 11 | Coffee Br. | |

## Detailed Program
### Monday 9:00 - 10:45 (Session 1)
**Welcome + Presentations.** (FM)
    The organizers present themselves. Participants who feel comfortable to do so present themselves and their work, 5 minutes each, with or without a projected presentation.

### Monday 11:00 - 12:00 (Session 2)
**Practice: Getting used to the environment** (PD)
    Introduction to the use of the development environment of the summer school and presentation of the provided GPU machines. Participants make sure they can execute GPU-based Palabos applications on their own hardware and/or on the provided GPU computers.

### Monday 13:30 - 15:00 (Session 3)
**Theory: Overview of Palabos on  GPU** (OM)
    General background on Palabos-on-GPU. Principles of GPU computing, history of porting Palabos, implementation and usage model.

**Practice: A first Palabos GPU application** (JL)
    The participants execute two provided Palabos examples than run both on CPU and on GPU: the 3D flow in a lid-driven cavity and a flow through a porous media. They read the code to get familiar with the (minor) additions required to warrant GPU execution. They run the code on a remote GPU machine, retrieve the data to their computer and post-process it with Paraview.

### Monday 15:15 - 17:00 (Session 4)
**Theory: Assessing GPU and multi-GPU performance** (PD)
    What performance do we expect to achieve on GPU ? And on multiple GPUs ? How does it compare to CPU performance ? How does it depend on the hardware ? And on the use of single- vs double-precision floats ? This presentation provides a general overview of performance metrics in GPU computing.

**Practice: Benchmarking a Palabos GPU application** (KT)
    Participants take up again the examples executed before and look into the performance, with and without output, identify bottlenecks, etc.

### Tuesday 9:00 - 10:45 (Session 5)
**Extending Palabos on GPU** (JL)
    In this presentation, participants learn how to adapt collision models ("dynamics objects") and data processors to run on GPUs.

**Practice: Introduction to the Palabos multi-component code** (FM)
    The carbon-sequestration code is presented. Through simple questions, participants make sure they understand the different components of the code.  At the end of the session, everyone is able to retrieve the VTK files and visualize the data.

### Tuesday 11:00 - 12:00 (Session 6)
**Practice: Implement pre- and post-processing capabilities** (RP)
    You apply the theory to practice and implement you own data processors to perform data analytics operations on the data produced by the carbon-sequestration code. 

### Tuesday 13:30 - 15:00 (Session 7) 
**Presentation of the carbon sequestration problem** (AP)
**Group work**
    Repartition into groups (6 groups)
    - One HPC group (maybe at least 2 GPUs on Muguet) (JL)
    - Dependence on contact angle (FM)
    - Dependence on interface tension (PD)
    - Dependence on injection rate / injection density (CC)
    - Dependence on viscosity ration between brine and CO2 (RP)
    - Dependence on the height of the porous media (KT)

### Tuesday 15:15 - 17:00 (Session 8) 
**Practice: Group work on carbon sequestration problem** (Moira + PD)
    The groups work on the asks assigned to them. They submit their jobs by the end of the afternoon to recover results on Wednesday morning.

### Tuesday 17:00 - 18:00 (Keynote talk)
   Prof. Xiaowen Shan: **The pseudo-potential model: a modern update**

### Wednesday 9:00 - 10:45  (Session 9)
**Background: an introduction to GPU programming using C++ standard parallelism** (RP)
    Independently of Palabos, we provide participants to the GPU programming approach used in GPU-based Palabos, based on C++ native language parallelism.

**Practice: Writing a GPU application from scratch** (KT)
    Exercise session: independently of Palabos, participants work on a GPU application using C++ native language parallelism.

### Wednesday 11:00 - 12:00 (Session 10)
 **Practice:** Group work (CC)
     The groups work on their project, evaluate the results of the submitted jobs, and prepare a presentation for the afternoon session.

### Wednesday 13:30 - 15:00 (Session 11)
**Practice**: Group Presentations (JL)
**Conclusion**: Final discussion
