/* This file is part of the Palabos library.
 *
 * The Palabos softare is developed since 2011 by FlowKit-Numeca Group Sarl
 * (Switzerland) and the University of Geneva (Switzerland), which jointly
 * own the IP rights for most of the code base. Since October 2019, the
 * Palabos project is maintained by the University of Geneva and accepts
 * source code contributions from the community.
 * 
 * Contact:
 * Jonas Latt
 * Computer Science Department
 * University of Geneva
 * 7 Route de Drize
 * 1227 Carouge, Switzerland
 * jonas.latt@unige.ch
 *
 * The most recent release of Palabos can be downloaded at 
 * <https://palabos.unige.ch/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file
  * Flow through a porous sandstone. Benchmark case for the Palabos
  * project "From CPU to GPU in 80 days".
  * Project page: https://palabos.unige.ch/community/cpu-gpu-80-days/
  * Performance measurements: https://docs.google.com/spreadsheets/d/1ROJbPlLKqX9JxO408S4BEFkzK1XLbxiimUdd4XaIJ8c/edit?usp=sharing
  *
  **/

#define USE_NVIDIA_HPC_SDK // Enables cudaMalloc for communication buffers and NVTX ranges.

#include "palabos3D.h"
#include "palabos3D.hh"
#include <memory>
#include <iostream>
#include <fstream>

using namespace plb;
using namespace std;

typedef float T;
#define DESCRIPTOR descriptors::D3Q19Descriptor

class IniCavityFunctional3D : public BoxProcessingFunctional3D_L<T, DESCRIPTOR>
{
public:
    IniCavityFunctional3D(Box3D const& fullDomain_, Array<T, 3> const& u_)
        : fullDomain(fullDomain_),
          u(u_)
    { }
    virtual void process(Box3D domain, BlockLattice3D<T, DESCRIPTOR>& lattice)
    {
        Box3D innerDomain = fullDomain.enlarge(-1);
        Dot3D relativeOffset = lattice.getLocation();
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            plint absX = iX + relativeOffset.x;
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                plint absY = iY + relativeOffset.y;
                for (plint iZ=domain.z0; iZ<=domain.z1; ++iZ) {
                    plint absZ = iZ + relativeOffset.z;

                    if (!contained(absX, absY, absZ, innerDomain)) {
                        HalfwayBounceBack<T, DESCRIPTOR>* dynamics = new HalfwayBounceBack<T, DESCRIPTOR>(
                                lattice.get(iX, iY, iZ).getDynamics().clone());
                        for (plint iPop = 0; iPop < DESCRIPTOR<T>::q; ++iPop) {
                            plint nextX = absX + DESCRIPTOR<T>::c[iPop][0];
                            plint nextY = absY + DESCRIPTOR<T>::c[iPop][1];
                            plint nextZ = absZ + DESCRIPTOR<T>::c[iPop][2];
                            if (contained(nextX, nextY, nextZ, fullDomain)) {
                                dynamics->getData(iPop) = std::numeric_limits<T>::signaling_NaN();
                            }
                            else {
                                if (nextZ > fullDomain.z1) {
                                    dynamics->setVelocity(iPop, u);
                                }
                                else{
                                    dynamics->setVelocity(iPop, Array<T, 3>(0., 0., 0.));
                                }
                            }
                        }
                        lattice.attributeDynamics(iX, iY, iZ, dynamics);
                    }
                }
            }
        }
    }

    virtual IniCavityFunctional3D* clone() const {
        return new IniCavityFunctional3D(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::dataStructure;
    }
private:
    Box3D fullDomain;
    Array<T, 3> u;
};

/// Compute the kinetic energy averaged over the whole domain.
template <class LATTICE>
auto computeAveragedKinEnergy(LATTICE& lattice)
{
    unique_ptr<MultiScalarField3D<T> > kinEnergy = computeKineticEnergy(lattice);
    return computeAverage(*kinEnergy);
}

int main(int argc, char* argv[])
{
    plbInit(&argc, &argv);
    // TODO: turn on blocking communication (see the tgv3d example).

    bool useAccelerated = true; // Use accelerated lattice (for GPU execution) ?

    // TODO: identify the below line which allows to run in benchmark mode or production mode (with output)
    bool benchmark = true;   // Run in benchmark mode ?

    bool production = !benchmark; // Run in production mode ?

    // Output VTK files go to this directory
    global::directories().setOutputDir("./tmp/");

    pcout << "Number of MPI threads: " << global::mpi().getSize() << endl;

    int maxIter = 200;  // Total number of iterations in benchmark mode
    int bench_ini_iter = 100; // Number of warm-up iterations in benchmark mode
    int vtkIter = 100;  // Frequency of VTK file output in production mode
    int statIter = 100;  // Frequency at which permeability is provided, in production mode

    int default_nx = 600; 
    int default_ny = 600;
    int default_nz = 600;

    int nx = default_nx;
    int ny = default_ny;
    int nz = default_nz;

    if (global::argc() == 1) {
        pcout << "No arguments provided, running with standard parameters." << std::endl;
    }
    else {
        try {
            if (global::argc() != 4) {
                throw PlbIOException("Wrong number of arguments.");
            }
            global::argv(1).read(nx); 
            global::argv(2).read(ny); 
            global::argv(3).read(nz); 
        }
        catch(PlbIOException& except) {
            pcout << except.what() << std::endl;
            pcout << "Error in the provided parameters. The syntax is: " << std::endl;
            pcout << (std::string)global::argv(0) << " nx ny nz" << std::endl;
            return -1;
        }
    }

    if (production) {
        maxIter = 1'000'000; // In production, the simulation goes on for a million iterations
    }

    T Re    = 100.0; // Reynolds number
    T ulb   = 4.e-2; // Velocity in lattice units
    T uPhys = 1.0;
    T dx    = 1.0 / (T)nx;
    T dt    = ulb / uPhys * dx;
    // Re = u * L / nu
    T nu_lb = ulb * (T)nx / Re;
    T tau = 3. * nu_lb + (T)0.5;

    // Set up the lattice, use TRT dynamics (otherwise, the permeability is viscosity dependent)
    MultiBlockLattice3D<T, DESCRIPTOR> lattice(nx, ny, nz, new TRTdynamics<T,DESCRIPTOR>(1. / tau) );
    lattice.periodicity().toggleAll(true);
    applyProcessingFunctional(new IniCavityFunctional3D(lattice.getBoundingBox(), Array<T, 3>(ulb, 0., 0.)),
                              lattice.getBoundingBox(), lattice);
    lattice.periodicity().toggleAll(false);

    pcout << "Executing a ";
    if (benchmark) {
        pcout << "benchmark ";
    }
    else {
        pcout << "production ";
    }
    pcout << "simulation on a domain size "
          << nx << " x " << ny << " x " << nz << endl;

    // Initialize by setting the inflow and outflow velocity everywhere
    initializeAtEquilibrium(lattice, lattice.getBoundingBox(), (T)1., Array<T,3>((T)0., (T)0., (T)0.));

    auto writeVTK = [&] (auto& lattice, int iT) {
        VtkImageOutput3D<T> vtkOut(createFileName("vtk", iT, 6), dx);
        vtkOut.writeData<3,T>(*computeVelocity(lattice), "velocity", dx/dt);
        const T rho0 = 1.0;
        vtkOut.writeData<T>(*add((T)-1., *computeDensity(lattice)), "pressure", dx*dx/(dt*dt) / (T)3. * rho0);
    };

    // Reset the clock
    global::timer("cavity").start();
    plint clock_iter = 0;

    //pcout << std::endl << "Here is what the \"collideAndStream\" command should look like: " << std::endl;
    //showTemplateArguments(lattice);
    //pcout << std::endl;

    pcout << "Creating accelerated lattice" << endl;
    AcceleratedLattice3D<T, DESCRIPTOR> *accLattice = nullptr;
    if (useAccelerated) {
        // TODO: add a line to create accLattice, copy-constructed from lattice.
    }

    if (benchmark) {
        pcout << "Now running " << bench_ini_iter << " warm-up iterations." << endl;
    }
    else {
        pcout << "Starting simulation" << endl;
    }
    for (int iT = 0; iT < maxIter; ++iT) {
        if (production && iT % vtkIter == 0) {
            pcout << "Writing data into a VTK file" << endl;
            writeVTK(*accLattice, iT);
        }
        if (production && iT % statIter == 0) {
            T avEnergy = useAccelerated ? computeAveragedKinEnergy(*accLattice)
                                        : computeAveragedKinEnergy(lattice);
            pcout << "At iteration " << iT << ", average energy = " << avEnergy << endl;
        }

        if (benchmark && iT == bench_ini_iter)  {
            pcout << "Now running " << maxIter - bench_ini_iter
                  << " benchmark iterations." << endl;
            global::timer("cavity").restart();
            clock_iter = 0;
        }
        
        if (useAccelerated) {
            // TODO: there is nothing to add here. However, once you have played with the code,
            // try replacing the below line by the following line
            //     accLattice -> collideAndStream()
            // and observe the performance difference with the original line.
            accLattice -> collideAndStream (
                CollisionKernel<T,DESCRIPTOR,
                                CollisionModel::TRT,
                                CollisionModel::HalfwayBounceBack__TRT>() );
        }
        else {
            lattice.collideAndStream();
        }
        ++clock_iter;
    }

    // Print program performance in MLUPS
    double elapsed = global::timer("cavity").stop();
    double mlups = static_cast<double>(lattice.getBoundingBox().nCells() * clock_iter) / elapsed * 1.e-6;
    pcout << "Performance: " << setprecision(4) << mlups << " MLUPS" << endl;
    pcout << endl;

    // Execute a regression test
    double reference_energy = 1.;
    if (nx == 256 && ny == 256 && nz == 256 && sizeof(T) == sizeof(float) && maxIter == 200) {
        reference_energy = 8.446989340882e-06;
    }
    else if (nx == 400 && ny == 400 && nz == 400 && sizeof(T) == sizeof(float) && maxIter == 200) {
        reference_energy = 6.913718607393e-06;
    }
    else if (nx == 420 && ny == 420 && nz == 420 && sizeof(T) == sizeof(float) && maxIter == 200) {
        reference_energy = 6.761440090486e-06;
    }
    else if (nx == 500 && ny == 500 && nz == 500 && sizeof(T) == sizeof(float) && maxIter == 200) {
        reference_energy = 6.241020855668e-06;
    }
    else if (nx == 600 && ny == 600 && nz == 600 && sizeof(T) == sizeof(float) && maxIter == 2000) {
        reference_energy = 1.630528640817e-05;
    }

    if (false) {
        pcout << "No regression test defined for current parameters." << endl;
    }
    else {
        T avEnergy = useAccelerated ? computeAveragedKinEnergy(*accLattice)
                                    : computeAveragedKinEnergy(lattice);
        pcout << "Regression test with energy = " << setprecision(13) << avEnergy;
        if (std::fabs(avEnergy - reference_energy) < 1.e-10) {
            pcout << ": OK" << endl;
        }
        else {
            pcout << ": FAILED" << endl;
            pcout << "Expected the value " << reference_energy << endl;
        }
    }

    delete accLattice;
    return 0;
}

