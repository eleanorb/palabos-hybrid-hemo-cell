## Sessions 2: Getting used to the environment

- Log onto the GPU machine assigned to you.
- Create a directory at your name, if it is not already created.
- Check out the palabos-hybrid GitLab project.
- Change into the directory `summer_school_22/session2/build`
- Type `export CXX=nvc++`
- Type `cmake ..`
- Type `make -j`
- Type `cd ..`
- Execute the `tgv3d` example

Take some time to read the eample and identify differences between an old-style Palabos code and this hybrid one.
