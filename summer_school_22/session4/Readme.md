## Session 4: Performance analysis

What is the performance of the GPU platform you are working on ? Try to find information on the internet, especially about the memory bandwidth.

Compute the maximum performance for a D3Q19 LBM simulation, under the limitations of the memory bandwidth, in single and double precision.

Measure the performance of the cavity and porous-media codes provided for Session 3. How do they compare with the performance predictions ? Try to explain.
