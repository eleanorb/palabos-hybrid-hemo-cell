// *****************************************************************************
// LAPLACE_STL Program
// Supporting code file for the GTC 2021 presentation
// " Fluid dynamics on GPUs with C++ Parallel Algorithms:
//   state-of-the-art performance through a hardware-agnostic approach "

// Copyright © 2021 University of Geneva
// Authors: Jonas Latt
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#include <algorithm>
#include <numeric>
#include <utility>
#include <functional>
#include <execution>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <chrono>
#include <cmath>
#include <omp.h>

using namespace std;
using namespace std::chrono;

const int N = 4096;               // The domain has a N x N resolution.
const double dx = 1. / (double) (N - 1); // Discrete cell size.
const double dt = dx*dx;          // Discrete time step.
const double D = 0.20;            // Diffusion constant.
const double Dconst = D * dt / (dx*dx);  // Precomputed numerical constant.
const double epsilon = 1.e-5;     // Steady-state convergence criterion.
const int output_frequency = 200; // Terminal output intervals as a number of iterations (-1 means no output).
const int image_frequency  = -1;   // Image output intervals as a number of iterations (-1 means no output).

// Helper functions

auto sqr = [](double x) { return x * x; };

void write_to_bmp(int N, double const* data, int iter,
                  double minval, double maxval);

// Set the initial and boundary condition.
void initialize(double* u_ptr, double* utmp_ptr, double dx) {
    // Parallel initialization of u.
    for_each(u_ptr, u_ptr + N*N, [u_ptr, dx](double& u) {
    //for_each(execution::par_unseq, u_ptr, u_ptr + N*N, [u_ptr, dx](double& u) {
            size_t i = &u - u_ptr;
            size_t iX = i / N;
            size_t iY = i % N;
            double x = (double)iX * dx;
            double y = (double)iY * dx;
            u = 0; // Initial value in bulk
            // Left boundary: A sine period.
            if (iX == 0) {
                u = sin(y * 2. * M_PI);
            }
            // Right boundary: A shifted sine period.
            else if (iX == N - 1) {
                u = sin(y * 2. * M_PI + M_PI / 2.);
            }
            // Top and bottom boundary: linear increase
            else if (iY == 0 || iY == N - 1) {
                u = x;
            }
        } );
    // utmp is a copy of u, as they are going to be used interchangeably
    // at even and odd time steps.
    copy(execution::par_unseq, u_ptr, u_ptr + N*N, utmp_ptr);
}

double iterate_openmp_column(double* u_ptr, double* utmp_ptr, double Dconst) {
    auto IND = [](size_t iX, size_t iY) { return iY + N * iX; };
    double l2 = 0.;
    #pragma omp parallel for reduction(+:l2)
    for (int iX = 1; iX < N-1; iX++) {
        for (int iY = 1; iY < N-1; iY++) {
            size_t i = IND(iX, iY);
            utmp_ptr[i] = u_ptr[i] * (1. - 4. * Dconst)
                           + Dconst * ( u_ptr[IND(iX-1, iY)] + u_ptr[IND(iX+1, iY)] +
                                        u_ptr[IND(iX, iY-1)] + u_ptr[IND(iX, iY+1)] );
            l2 += sqr(utmp_ptr[i] - u_ptr[i]);
        }
    }
    return l2;
}

// One iteration step, using the formalism of lambda functions.
double iterate_lambda(double* u_ptr, double* utmp_ptr, double Dconst) {
    return
    transform_reduce (
        execution::par,
        u_ptr, u_ptr + N*N, utmp_ptr,
        0., plus<double>(),
        [u_ptr, Dconst](double const& u_from, double& u_to) {
            size_t i = &u_from - u_ptr;
            size_t iX = i / N;
            size_t iY = i % N;
            bool on_boundary_x = iX == 0 || iX == N-1;
            bool on_boundary_y = iY == 0 || iY == N-1;
            if (on_boundary_x ||on_boundary_y) {
                return 0.;
            }
            else {
                auto IND = [](size_t iX, size_t iY) { return iY + N * iX; };
                u_to = u_from * (1. - 4. * Dconst)
                               + Dconst * ( u_ptr[IND(iX-1, iY)] + u_ptr[IND(iX+1, iY)] +
                                            u_ptr[IND(iX, iY-1)] + u_ptr[IND(iX, iY+1)] );
                return sqr(u_to - u_from);
            }
        } );
}

// A class for functions objects, which are an explicit version of the anonymous
// object generated by the lambda function above.
// This implementation assumes element-wise parallelism. It uses pointer arithmetics
// to know the indices.

/**************
 * 
 * CODE HERE 
 * 
 */

// A class for functions objects, which are an explicit version of the anonymous
// object generated by the lambda function above.
// This implementation assumes element-wise parallelism. It uses pointer arithmetics
// to know the indices.
struct ColumnIteration {
    double* u_ptr;
    double Dconst;

    size_t IND (size_t iX, size_t iY) {
        return iY + N * iX;
    }

    double operator()(double const* column_from, double* column_to) {
        size_t i = column_from - u_ptr;
        size_t iX = i / N;
        double l2 = 0.;
        for (size_t iY = 1; iY < N-1; ++iY) {
            column_to[iY] = column_from[iY] * (1. - 4. * Dconst)
                       + Dconst * ( u_ptr[IND(iX-1, iY)] + u_ptr[IND(iX+1, iY)] +
                                    column_from[iY-1] + column_from[iY+1] );
            l2 += sqr(column_to[iY] - column_from[iY]);
        }
        return l2;
    }
};

void printUsage(char* argv[]) {
    cout << "Usage: \"" << argv[0] << " [implementation]\", where implementation is one of the following:" << endl;
    cout << " - openmp_column: Column-based parallelism with OpenMP (remember to set the OMP_NUM_THREADS environment variable)." << endl;
    cout << " - paralg_element: Element-level application of parallel algorithms." << endl;
    cout << " - paralg_column: Column-level application of parallel algorithms." << endl;
}

int main(int argc, char* argv[]) {

    if (argc != 2) {
        cout << "Wrong number of arguments. One argument expected." << endl;
        printUsage(argv);
        return -1;
    }

    string user_arg(argv[1]);

    enum class Implementation {
        openmp_column,
        paralg_element,
        paralg_column
    }
    implementation { Implementation::openmp_column };

    if (user_arg == "openmp_column") {
        implementation = Implementation::openmp_column;
    }
    else if (user_arg == "paralg_element") {
        implementation = Implementation::paralg_element;
    }
    else if (user_arg == "paralg_column") {
        implementation = Implementation::paralg_column;
    }
    else {
        cout << "Error: \"" << user_arg << "\" is not a valid implementation scheme." << endl;
        printUsage(argv);
        return -1;
    }

    // Allocation of memory
    vector<double> u(N*N), utmp(N*N);
    double* u_ptr = &u[0];
    double* utmp_ptr = &utmp[0];
    vector<double*> columns(N), columns_tmp(N);
    for (int iX = 0; iX < N; ++iX) {
        columns[iX] = u_ptr + N * iX;
        columns_tmp[iX] = utmp_ptr + N * iX;
    }
    double** columns_ptr = &columns[0];
    double** columns_tmp_ptr = &columns_tmp[0];

    initialize(u_ptr, utmp_ptr, dx);

    int num_iter = 0;
    int num_image = 0;
    double l2 = 1.;
    auto start = high_resolution_clock::now();
    while(l2 > epsilon) {
        switch(implementation) {
            case Implementation::openmp_column:
                l2 = iterate_openmp_column(u_ptr, utmp_ptr, Dconst);
                break;
            case Implementation::paralg_element:
                //l2 = iterate_lambda(u_ptr, utmp_ptr, Dconst);
                l2 = transform_reduce( execution::par_unseq, begin(u), end(u), begin(utmp), 0., plus<double>(),
                                       ElementIteration {u_ptr, Dconst} );
                break;
            case Implementation::paralg_column:
                l2 = transform_reduce( execution::par_unseq, columns_ptr + 1, columns_ptr + N-1, columns_tmp_ptr + 1, 0., plus<double>(),
                                       ColumnIteration {u_ptr, Dconst} );
                break;
            default: return -1;
        }

        num_iter++;
        swap(u_ptr, utmp_ptr);
        swap(columns_ptr, columns_tmp_ptr);
        u.swap(utmp);
        columns.swap(columns_tmp);

        if (output_frequency != -1 && num_iter % output_frequency == 0) {
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<microseconds>(stop - start);
            double giga_flops = (double)(N*N) * (double)num_iter * 9. / (double)duration.count() * 1.e-3;
            cout << "At step " << num_iter << ", l2 = " << setprecision(5) << sqrt(l2)
                 << "; performance = " << giga_flops << " GFLOPS" << endl;
        }
        if (image_frequency != -1 && num_iter % image_frequency == 0) {
            write_to_bmp(N, u_ptr, num_image++, -1., 1.);
        }
    } 

    return 0;
}


// Write the data to an image in the raw BMP format.
// To convert images to a video: ffmpeg -r 40 -i T_%04d.bmp -q:v 0 heat.mp4
void write_to_bmp(int N, double const* data, int iter, double minval, double maxval)
{
    unsigned char bmpfileheader[14] = {'B','M',0,0,0,0,0,0,0,0,54,0,0,0};
    unsigned char bmpinfoheader[40] = { 40, 0,  0, 0,
                                0, 0,  0, 0,
                                0, 0,  0, 0,
                                1, 0, 24, 0 };

    int width = N;
    int height = N;

    int padding = (4 - (width * 3) % 4) % 4;

    int datasize = (3 * width + padding) * height;
    int filesize = 54 + datasize;

    vector<unsigned char> img(datasize);

    // A linear interpolation function, used to create the color scheme.
    auto linear = [](double x, double x1, double x2, double y1, double y2)
       {
           return ( (y2-y1) * x + x2*y1 - x1*y2 ) / (x2-x1);
       };

    for(int iX = 0; iX < width; iX++){
        for(int iY = 0; iY < height; iY++){
            // Restrain the value to be plotted to [0, 1]
            double value = ((data[iY + height * iX] - minval) / (maxval - minval));
            double r = 0., g = 0., b = 0.;
            // For good visibility, use a color scheme that goes from black-blue to black-red.
            if (value <= 1./8.) {
                r = 0.;
                g = 0.;
                b = linear(value, -1./8., 1./8., 0., 1.);
            }
            else if (value <= 3./8.) {
                r = 0.;
                g = linear(value, 1./8., 3./8., 0., 1.);
                b = 1.;
            }
            else if (value <= 5./8.) {
                r = linear(value, 3./8., 5./8., 0., 1.);
                g = 1.;
                b = linear(value, 3./8., 5./8., 1., 0.);
            }
            else if (value <= 7./8.) {
                r = 1.;
                g = linear(value, 5./8., 7./8., 1., 0.);
                b = 0.;
            }
            else {
                r = linear(value, 7./8., 9./8., 1., 0.);
                g = 0.;
                b = 0.;
            }

            r = min(255. * r, 255.);
            g = min(255. * g, 255.);
            b = min(255. * b, 255.);

            img[(iX + iY*width)*3 + iY*padding + 2] = (unsigned char)(r);
            img[(iX + iY*width)*3 + iY*padding + 1] = (unsigned char)(g);
            img[(iX + iY*width)*3 + iY*padding + 0] = (unsigned char)(b);
        }
    }

    bmpfileheader[ 2] = (unsigned char)(filesize      );
    bmpfileheader[ 3] = (unsigned char)(filesize >>  8);
    bmpfileheader[ 4] = (unsigned char)(filesize >> 16);
    bmpfileheader[ 5] = (unsigned char)(filesize >> 24);

    bmpinfoheader[ 4] = (unsigned char)(       width      );
    bmpinfoheader[ 5] = (unsigned char)(       width >>  8);
    bmpinfoheader[ 6] = (unsigned char)(       width >> 16);
    bmpinfoheader[ 7] = (unsigned char)(       width >> 24);
    bmpinfoheader[ 8] = (unsigned char)(       height      );
    bmpinfoheader[ 9] = (unsigned char)(       height >>  8);
    bmpinfoheader[10] = (unsigned char)(       height >> 16);
    bmpinfoheader[11] = (unsigned char)(       height >> 24);
    bmpinfoheader[20] = (unsigned char)(datasize      );
    bmpinfoheader[21] = (unsigned char)(datasize >>  8);
    bmpinfoheader[22] = (unsigned char)(datasize >> 16);
    bmpinfoheader[23] = (unsigned char)(datasize >> 24);

    stringstream filename;
    filename << "T_";
    filename << setfill('0') << setw(4) << iter;
    filename << ".bmp";

    ofstream f;
    f.open(filename.str().c_str(), ios::binary | ios::out);

    f.write((const char*)bmpfileheader, 14);
    f.write((const char*)bmpinfoheader, 40);

    f.write((const char*)&img[0], datasize);
    f.close();
}
