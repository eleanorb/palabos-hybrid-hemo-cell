/* This file is part of the Palabos library.
* Copyright (C) 2009 Jonas Latt
* E-mail contact: jonas@lbmethod.org
* The most recent release of Palabos can be downloaded at 
* <http://www.lbmethod.org/palabos/>
*
* The library Palabos is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation, either version 3 of the
* License, or (at your option) any later version.
*
* The library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file
  * Taylor-Green vortex.
  * This benchmark case for the Palabos project "From CPU to GPU in 80 days" was added by Christophe Coreixas.
  * Project page: https://palabos.unige.ch/community/cpu-gpu-80-days/
  * Performance measurements: https://docs.google.com/spreadsheets/d/1ROJbPlLKqX9JxO408S4BEFkzK1XLbxiimUdd4XaIJ8c/edit?usp=sharing
  *
  **/

#define USE_ACC

#ifdef USE_ACC
    #define MULTIBLOCK AcceleratedLattice3D
#else
    #define MULTIBLOCK MultiBlockLattice3D
#endif

#define USE_NVIDIA_HPC_SDK // Enables cudaMalloc for communication buffers and NVTX ranges. 

#include "palabos3D.h"
#include "palabos3D.hh"   // include full template code
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <chrono>
#include "omp.h"

using namespace plb;
using namespace plb::descriptors;
using namespace std;
using namespace std::chrono;

typedef float T;
#define DESCRIPTOR D3Q19Descriptor


//////// A functional, used to get the dynamics
template<typename T, template<typename U> class Descriptor>
Dynamics<T,Descriptor> *getDynamics(string &dynName, T &omega)
{
    Dynamics<T,Descriptor> *dyn;
    if (dynName == "BGK") { 
        dyn = new BGKdynamics<T,Descriptor>(omega);
    } else if (dynName == "RM") { 
        dyn = new RMdynamics<T,Descriptor>(omega);
    } else if (dynName == "HM") { 
        dyn = new HMdynamics<T,Descriptor>(omega);
    } else if (dynName == "CM") { 
        dyn = new CMdynamics<T,Descriptor>(omega);
    } else if (dynName == "CHM") { 
        dyn = new CHMdynamics<T,Descriptor>(omega);
    } else if (dynName == "K") { 
        dyn = new Kdynamics<T,Descriptor>(omega);
    } else if (dynName == "GH") { 
        dyn = new GHdynamics<T,Descriptor>(omega);
    } else if (dynName == "RR") { 
        dyn = new RRdynamics<T,Descriptor>(omega);
    } else {
        pcout << "Error: dynamics name does not exist." << std::endl;
        exit(-1);
    }

    return dyn;
}


//////// A functional, used to initialize the simulation
template<typename T>
class TGVInitialVelocityField {
public:
    TGVInitialVelocityField(plint const& nx_, plint const& ny_, plint const& nz_,
        T const& u0_)
        : nx(nx_), ny(ny_),  nz(nz_), u0(u0_)
    { }

    void operator()(plint iX, plint iY, plint iZ, T &rho, Array<T,3>& u) const {

        T rho0 = 1.0;
        T U0 = u0;
        T pi = M_PI;

        T x = (T)2.*pi*(iX-nx/2.)/T(nx);
        T y = (T)2.*pi*(iY-ny/2.)/T(ny);
        T z = (T)2.*pi*(iZ-nz/2.)/T(nz);

        rho  = rho0 * ( 1. + U0*U0/(16.* DESCRIPTOR<T>::cs2)*(cos(2.*x) + cos(2.*y))*(cos(2.*z) + 2.) );
        u[0] =  U0*sin(x)*cos(y)*cos(z);
        u[1] = -U0*cos(x)*sin(y)*cos(z);
        u[2] = 0.;
    }
    
private:
    plint nx;
    plint ny;
    plint nz;
    T u0;
};


//////// Initialize the simulation
void simulationSetup(MULTIBLOCK<T,DESCRIPTOR>& lattice,
                     const plint nx, const plint ny, const plint nz,
                     const T u0)
{
    // Set periodic boundaries.
    lattice.periodicity().toggleAll(true); 

    // Initialize the simulation domain.
    initializeAtEquilibrium (
        lattice, lattice.getBoundingBox(),
        TGVInitialVelocityField<T>(nx, ny, nz, u0) );

    // Call initialize to get the lattice ready for the simulation.
    lattice.initialize();
}


//////// Post processing & Data outputs
/// Write 3D fields (physical units) into a VTK file.
void writeVTK(MULTIBLOCK<T,DESCRIPTOR>& lattice,
              T dx, T dt, plint iter)
{
    VtkStructuredImageOutput3D<T> vtkOut(createFileName("vtk", iter, 6), dx);
    vtkOut.writeData<T>(*computeVelocityNorm(lattice), "machNorm", 1.0/std::sqrt(DESCRIPTOR<T>::cs2));
    vtkOut.writeData<T>(*computeDensity(lattice), "density", 1.0);
    vtkOut.writeData<3,T>(*computeVelocity(lattice), "velocity", dx/dt);
    vtkOut.writeData<3,T>(*computeBulkVorticity(*computeVelocity(lattice)), "vorticity", (T)1/dt);
}

/// Compute the compressible kinetic energy averaged over the whole domain.
/// (accounts for density variations) 
auto computeAveragedCompressibleKinEnergy(MULTIBLOCK<T,DESCRIPTOR>& lattice)
{
    unique_ptr<MultiScalarField3D<T> > density = computeDensity(lattice);
    unique_ptr<MultiScalarField3D<T> > velocityNorm = computeVelocityNorm(lattice);
    unique_ptr<MultiScalarField3D<T> > kinEnergy = 
        multiply((T)0.5, *multiply(*density,*multiply(*velocityNorm,*velocityNorm)));
    return computeAverage(*kinEnergy);
}

template<typename T, template<class U> class Descriptor>
class TotalKineticEnergyFunctional3D : public ReductiveBoxProcessingFunctional3D_A<T,Descriptor>
{
public:
    TotalKineticEnergyFunctional3D();
    virtual void process(Box3D domain, AtomicAcceleratedLattice3D<T,Descriptor>& lattice);
    virtual TotalKineticEnergyFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    T getTotalEnergy() const;
private:
    plint totalEnergyId;
};

template<typename T, template<class U> class Descriptor>
TotalKineticEnergyFunctional3D<T,Descriptor>::TotalKineticEnergyFunctional3D()
    : totalEnergyId(this->getStatistics().subscribeSum())
{ }

template<typename T, template<class U> class Descriptor>
void TotalKineticEnergyFunctional3D<T,Descriptor>::process (
        Box3D domain, AtomicAcceleratedLattice3D<T,Descriptor>& lattice)
{
    T sum_usqr =
    lattice.for_each_reduce(domain, (T)0, std::plus<T>(),
         [&lattice](plint i, plint iX, plint iY, plint iZ, int collisionModel)
         {
            T rhoBar;
            Array<T,Descriptor<T>::d> j;
            Array<T, Descriptor<T>::q> f;
            lattice.pullPop(i, f);
            momentTemplatesImpl<T, typename Descriptor<T>::BaseDescriptor>::get_rhoBar_j(f, rhoBar, j);
            T rho = Descriptor<T>::fullRho(rhoBar);
            Array<T, 3> u(j[0] / rho, j[1] / rho, j[2] / rho);
            return u[0] * u[0] + u[1] * u[1] + u[2] * u[2];
        });
    this->getStatistics().gatherSum(totalEnergyId, sum_usqr * 0.5);
}

template<typename T, template<class U> class Descriptor>
T TotalKineticEnergyFunctional3D<T,Descriptor>::getTotalEnergy() const
{
    return this->getStatistics().getSum(totalEnergyId);
}

template<typename T, template<class U> class Descriptor>
TotalKineticEnergyFunctional3D<T,Descriptor>* TotalKineticEnergyFunctional3D<T,Descriptor>::clone() const
{
    return new TotalKineticEnergyFunctional3D<T,Descriptor>(*this);
}

template<typename T, template<class U> class Descriptor>
void TotalKineticEnergyFunctional3D<T,Descriptor>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::nothing;
}

T computeTotalEnergy(AcceleratedLattice3D<T, DESCRIPTOR>& lattice)
{
    TotalKineticEnergyFunctional3D<T, DESCRIPTOR> functional;
    applyProcessingFunctional(functional, lattice.getBoundingBox(), lattice);
    return functional.getTotalEnergy();
}


/// Compute the kinetic energy averaged over the whole domain.
auto computeAveragedKinEnergy(MULTIBLOCK<T,DESCRIPTOR>& lattice)
{
    //return  computeTotalEnergy(lattice) / (T)lattice.getBoundingBox().nCells();
    unique_ptr<MultiScalarField3D<T> > kinEnergy = computeKineticEnergy(lattice);
    return computeAverageAcc(*kinEnergy);
    //return computeAverage(*kinEnergy);
}

/// Compute the enstrophy averaged over the whole domain.
auto computeAveragedEnstrophy(MULTIBLOCK<T,DESCRIPTOR>& lattice)
{
    unique_ptr<MultiTensorField3D<T,3> > velocity = computeVelocity(lattice);
    unique_ptr<MultiTensorField3D<T,3> > vorticity = computeBulkVorticity(*velocity);
    unique_ptr<MultiScalarField3D<T> > vorticityNorm = computeNorm(*vorticity);
    unique_ptr<MultiScalarField3D<T> > enstrophy = 
        multiply((T)0.5, *multiply(*vorticityNorm,*vorticityNorm) );
    return computeAverage(*enstrophy);
}

/// Write all parameters in a log file.
void writeLogFile(string &dynName, T& Re, T& Ma, T& soundSpeed, plint& N, 
    plint& nx, plint& ny, plint& nz, T& dx, T& dt, T& u0, 
    T& nu, T& tau, T& omega, T& tc)
{
    plb_ofstream fout("tmp/log.dat");
    fout << " //=========== LBM Parameters ============// " << std::endl;
    fout << "   Lattice  -->           "<< "D3Q19" << std::endl;
    fout << "   Dynamics -->           "<< dynName << std::endl;
    fout << std::endl;

    fout << " //========= Physical Parameters =========// " << std::endl;
    fout << " Flow properties (dimensionless):    " << std::endl;
    fout << "   Re = " << Re << std::endl;
    fout << "   Ma = " << Ma << std::endl;
    fout << " Flow properties (physical units):    " << std::endl;
    fout << "   nu = " << nu*dx*dx/dt << " [m2/s]" << std::endl;
    fout << "   c  = " << soundSpeed << " [m/s]" << std::endl;
    fout << "   u0 = " << Ma * ::sqrt(DESCRIPTOR<T>::cs2) * dx/dt << " [m/s]" << std::endl;
    fout << "   tc = " << tc * dt << " [s]" << std::endl;
    fout << " Geometry (physical units):    " << std::endl;
    fout << "   lx = " << nx*dx << " [m]" << std::endl;
    fout << "   ly = " << ny*dx << " [m]" << std::endl;
    fout << "   lz = " << nz*dx << " [m]" << std::endl;
    fout << std::endl;

    fout << " //======== Numerical Parameters =========// " << std::endl;
    fout << " Numerical discretization (physical units):    " << std::endl;        
    fout << "   dx = " << dx << " [m]" << std::endl;
    fout << "   dt = " << dt << " [s]" << std::endl;
    fout << " Geometry (LB units):    " << std::endl;
    fout << "   N  = " << N << " (resolution)" << std::endl;
    fout << "   nx = " << nx << std::endl;
    fout << "   ny = " << ny << std::endl;
    fout << "   nz = " << nz << std::endl;
    fout << " Flow properties (LB units):    " << std::endl;
    fout << "   nuLB = " << nu << std::endl;
    fout << "   u0LB = " << Ma * ::sqrt(DESCRIPTOR<T>::cs2) << std::endl;
    fout << "   tcLB = " << round(tc) << " (" << tc << ")" << std::endl;
    fout << " Collision parameters (LB units):    " << std::endl;
    fout << "   tau = " << tau << std::endl;
    fout << "   omega = " << omega << std::endl;
    fout << std::endl;

    fout << " //======== Simulation parameters ========// " << std::endl;
    fout << "   output= " << "tmp" << std::endl;
    fout << "   tAdim = " << "20" << " * tc" << std::endl;
    fout << "         = " << (plint)(20 * tc) * dt << " [s]" << std::endl;
    fout << "         = " << (plint)(20 * tc) << " [iterations]" << std::endl;
    fout << "   vtsT  = " << "5" << " * tc" << std::endl;
    fout << "         = " << (plint)(5 * tc) * dt << " [s]" << std::endl;
    fout << "         = " << (plint)(5 * tc) << " [iterations]" << std::endl;
}

// Return a new clock for the current time, for benchmarking.
auto restartClock() {
    return make_pair(high_resolution_clock::now(), 0);
}

// Compute the time elapsed since a starting point, and the corresponding
// performance of the code in Mega Lattice site updates per second (MLups).
T printMlups(plint clock_iter, plint nelem) {
    T elapsed = global::timer("tgv").stop();
    T mlups = static_cast<T>(nelem * clock_iter) / elapsed * 1.e-6;

    pcout << "Benchmark result: " << setprecision(4) << mlups << " MLUPS" << endl;
    pcout << endl;
    return mlups;
}

// Run a regression test for a specific pre-recorded value 
// of the average kinetic energy.
void runRegression(T energy, plint iT, string dynName, int N, int sizeofT) {
    T reference_energy = 0.;
    if (dynName == "RR" && N == 256 && iT==200 && sizeofT == sizeof(float)) {
        reference_energy = 827.9800415039;
    }
    else if (dynName == "BGK" && N == 256 && iT==200 && sizeofT == sizeof(float)) {
        reference_energy = 838.2340698242;
    }
    else {
        pcout << "No regression value provided for your parameter set." << endl;
        return;
    }

    //if (dynName == "TRT") reference_energy = 104.7253828509;
    //else if (dynName == "RM" || dynName == "HM" || dynName == "CM" || dynName == "CHM") reference_energy = 104.725421212;
    //else if (dynName == "K") reference_energy = 104.725421212;
    //else if (dynName == "GH") reference_energy = 104.7254276103;
    //else if (dynName == "RR") reference_energy = 104.725421212;
    pcout << "Regression test at iteration " << iT << ": Average energy = "
         << setprecision(13) << energy;
    if (std::fabs(energy - reference_energy) < 1.e-10) {
        pcout << ": OK" << endl;
    }
    else {
        pcout << ": FAILED" << endl;
        pcout << "Expected the value " << reference_energy << endl;
    }
}


//////// MAIN PROGRAM
int main(int argc, char* argv[]) {
    plbInit(&argc, &argv);
    defaultMultiBlockPolicy3D().toggleBlockingCommunication(false);

    //////// Collision model
    string dynName = "RR";

    //////// Benchmark parameters
    bool regression = true;
    bool benchmark = true;
    plint bench_ini_iter = 100;
    plint bench_max_iter = 200;

    //////// Simulation domain parameters
    plint N = 256;
    plint nx = N; plint ny = N; plint nz = N;

    //////// Dimensionless parameters
    T u0 = 0.02;                                // Velocity in LB units
    T cs = ::sqrt(DESCRIPTOR<T>::cs2);          // Sound speed in LB units
    T Ma = u0 / cs;                             // Mach number
    T Ntgv = N/(2.*M_PI);                       // The normalized size of the domain is 2*pi in paper, 
                                                // but here we used 1 instead, hence the normalization
    T tc = (T)(Ntgv/u0);                        // Convective time in iterations
    T Re = 1600.;                               // Reynolds number
    T nu = (u0*Ntgv)/Re;                        // Kinematic viscosity in LB units
    T tau = nu/DESCRIPTOR<T>::cs2;              // Relaxation time in LB units
    T omega = 1./(tau + 0.5);                   // Relaxation frequency in LB units

    //////// Numerical parameters and sound speed
    
    ///// Convective scaling based on an arbitrary velcoity of 1 [m/s]
    // T dx = 0.01;                                // Space step in physical units [m]
    // T dt = dx * u0;                             // Time step in physical units [s] based on the convective scaling 
    //                                             //   this is different than imposing dt with the speed of sound!!!
    //                                             //   Instead, here we assume that u_phy = 1 [m/s] 
    // T soundSpeed = cs*(dx/dt);                  // Sound speed obtained with u_phy = 1 [m/s]

    ///// Acoustic scaling based on the speed of sound for air at 1013.25 hPa and 20°C 
    T soundSpeed = 343.;                           // Sound speed in physical units [m/s]
    T dx = 0.01;                                   // Space step in physical units [m]
    T dt = dx * (cs/soundSpeed);                   // Time step in physical units [s] based on the acoustic scaling 


    ///// Output directory
    global::directories().setOutputDir("tmp/");

    ///// Simulation maximal time, and output frequency (in terms of iterations).
    plint vtkTout = 5 * tc;
    plint tmax = benchmark ? bench_max_iter : (plint) 20 * tc + 1; // If benchmark mode, we only run bench_max_iter iterations

    // Print the simulation parameters to the terminal.
    if (benchmark) pcout << "Taylor-Green vortex, benchmark mode" << endl;
    else pcout << "Taylor-Green vortex, production mode" << endl;

    if (sizeof(T) == sizeof(float)) {
        pcout << "Running single precision" << endl;
    }
    else {
        pcout << "Running double precision" << endl;
    }
    pcout << "Size = {" << nx << ", " << ny << ", " << nz << "}" << endl;
    pcout << "Collision = " << dynName << endl;
    pcout << "Re = " << Re << endl;
    pcout << "omega = " << omega << endl;
    pcout << "u0 = " << u0 << endl;
    if (benchmark) {
        pcout << "Now running " << bench_ini_iter << " warm-up iterations." << endl;
    }
    else {
        pcout << "max_t = " << tmax << endl;
    }

    ///// Generate the dynamics.
    Dynamics<T,DESCRIPTOR> *dyn = getDynamics<T,DESCRIPTOR>(dynName, omega);
    ///// Generate and initialize relaxation parameters for MRT approaches.
    Array<T, DESCRIPTOR<T>::numRelaxationTimes> relaxMatrix;
    for (int i = 0; i < DESCRIPTOR<T>::numRelaxationTimes; ++i) relaxMatrix[i] = omega;
    if (dynName == "RM") { 
        RMdynamics<T,DESCRIPTOR>::allOmega = relaxMatrix;
    } else if (dynName == "HM") { 
        HMdynamics<T,DESCRIPTOR>::allOmega = relaxMatrix;
    } else if (dynName == "CM") { 
        CMdynamics<T,DESCRIPTOR>::allOmega = relaxMatrix;
    } else if (dynName == "CHM") { 
        CHMdynamics<T,DESCRIPTOR>::allOmega = relaxMatrix;
    } else if (dynName == "K") { 
        Kdynamics<T,DESCRIPTOR>::allOmega = relaxMatrix;
    } else if (dynName == "GH") { 
        GHdynamics<T,DESCRIPTOR>::allOmega = relaxMatrix;
    } else if (dynName == "RR") { 
        RRdynamics<T,DESCRIPTOR>::allOmega = relaxMatrix;
    }
    ///// Generate the lattice for the given dynamics.
    MULTIBLOCK<T, DESCRIPTOR> lattice (nx, ny, nz, dyn);

#ifdef USE_ACC
    // Activate the following line to use the accelerated lattice in OpenMP mode (multi-threaded CPU).
    //lattice.setExecutionMode(ExecutionMode::openmp);
#endif

    lattice.toggleInternalStatistics(false);

    ///// Initialization from analytical profiles
    simulationSetup(lattice, nx, ny, nz, u0);

    ///// Initial state is saved
    if (!regression && !benchmark) {
        writeVTK(lattice, dx, dt , 0);    
    }
    
    ///// Output stats (kinetic energy and enstrophy)      
    plb_ofstream statsOut("tmp/stats.dat");
    T previous_energy = std::numeric_limits<T>::max();

    ///// Output all parameters in a log file
    writeLogFile(dynName, Re, Ma, soundSpeed, N, nx, ny, nz, dx, dt, u0, 
        nu, tau, omega, tc);

    // Reset the clock.
    global::timer("tgv").start();

    plint clock_iter = 0;

    ///// Main loop over time iterations.
    plint iT = 0;
    for (iT=0; iT<tmax; ++iT) {
        // Here we are outputting data every 0.1*tc (approx 100 it for ulb = 0.02, Re = 1600 and N = 12)
        // so that we do not drastically impact performance (i.e., we only increase the simulation time 
        // by ~10% as compared to outputing data every tc, whereas outputing data every 0.05*tc would 
        // double it...)
        if (!regression && !benchmark && ((iT == 0) || (iT % (int)(0.1*tc) == 0)) || (iT == tmax)){
            T kinEnergy = computeAveragedKinEnergy(lattice);
            T kinEnergyAdim = kinEnergy/(0.5*u0*u0);

            T enstrophy = computeAveragedEnstrophy(lattice);
            T enstrophyAdim = enstrophy*(nx*nx)/(0.5*u0*u0);

            statsOut << iT/tc << " " 
                     << kinEnergy << " " << kinEnergyAdim << " " 
                     << enstrophy << " " << enstrophyAdim << std::endl;    

            ///// Stability test based on the decrease of the kinetic energy.
            // The kinetic energy should be smaller than its initial value (weak condition)
            if (iT == 0) previous_energy = kinEnergy;
            if ((iT > (int)(0.01*tc)) && !(kinEnergy < previous_energy)) {
                pcout << "Catastrophic error: energy has increased or is NaN!" << std::endl;
                return 1;
            }
        }

        if (benchmark && iT == bench_ini_iter)  {
            pcout << "Now running " << bench_max_iter - bench_ini_iter
                  << " benchmark iterations." << endl;
            global::timer("tgv").restart();
            clock_iter = 0;
        }

        ///// Lattice Boltzmann iteration step.
#ifdef USE_ACC
        //// The command below compiles all collision model kernels, 
        //// hence avoiding user typos but it is also less efficient
        // lattice.collideAndStream();
        
        // This only compiles required collision models
        lattice.collideAndStream (
           CollisionKernel<T, DESCRIPTOR, CollisionModel::RR>() );
#else
        lattice.collideAndStream();
#endif

        ///// Output.
        if (!regression && !benchmark && iT % vtkTout == 0) {
            pcout << "Writing VTK file at iteration = " << iT << std::endl;
            writeVTK(lattice, dx, dt, iT);
        }
        ++clock_iter;

    }
    statsOut.close();
    if (benchmark) {
        printMlups(clock_iter, nx*ny*nz);
    }

    if (regression) {
        T kinEnergy = nx*ny*nz*computeAveragedKinEnergy(lattice);
        // T kinEnergy = computeAveragedKinEnergy(lattice);
        runRegression(kinEnergy, iT, dynName, N, sizeof(T));
    }


    return 0;
}
